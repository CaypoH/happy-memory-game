from kivy.app import App
from kivy.lang import Builder
from kivy.base import EventLoop
from screenmaster import ScreenMaster


Builder.load_file('gamescreen.kv')
Builder.load_file('startscreen.kv')
Builder.load_file('achievescreen.kv')
Builder.load_file('gameoverscreen.kv')


class MemoryApp(App):
    def __init__(self, **kwargs):
        super(MemoryApp, self).__init__(**kwargs)

    def build(self):
        sm = ScreenMaster()
        self.manager = sm
        return sm

    def on_start(self):
        EventLoop.window.bind(on_keyboard=self.hook_keyboard)
        # this is for mobiles with 16:10 screen ratio
        if self.root_window.width / float(self.root_window.height) == 0.625:
            self.manager.startscreen.seticon.size_hint_y = .10
            self.manager.startscreen.achieveicon.size_hint_y = .10

    def on_pause(self):
        return True

    def hook_keyboard(self, window, key, *largs):
        if key == 27:
            if self.manager.get_current_screen() == 'start':
                self.manager.startscreen.quit.open()
                return True
            if self.manager.get_current_screen() == 'game' or self.manager.get_current_screen() == 'gameover':
                self.manager.go_to_start()
                self.manager.ids.game_screen.cards.clear_widgets()
                self.manager.gameoverscreen.ids.new_achieve_grid.clear_widgets()
                self.manager.startscreen.clear_game_state()
                self.manager.startscreen.ads_close_banner()
                return True
            if self.manager.get_current_screen() == 'achieve':
                self.manager.go_to_start()
                self.manager.achievescreen.achievegrid.clear_widgets()
                return True
            return True

if __name__ == '__main__':
    MemoryApp().run()