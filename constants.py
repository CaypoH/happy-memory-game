# folder to look for cards images
CARDS_IMAGES_FOLDER = 'images/'

# this one have to be inside of CARDS_IMAGES_FOLDER folder
CARD_BACK = 'backd.png'

# folder with all other game images
IMAGES_FOLDER = 'img/'

# folder with all game fonts
FONTS_FOLDER = 'fonts/'

# this one have to be inside of FONTS_FOLDER folder
MAIN_FONT = 'WickedMouse.otf'

# folder with all game sounds
MUSIC_FOLDER = 'sound/'

# this one have to be inside of MUSIC_FOLDER folder
BACKGROUND_MUSIC = 'island_0.ogg'

# link to configuration file
CONFIG_FILE = 'memory.ini'

# AdBuddiz publisher key
#PUBLISHER_KEY = '42e288cb-0a56-4851-a7ce-ea2450904090'

# AdMob Application ID
ADMOB_APP_ID = "ca-app-pub-1032896526805063/4974041345"

# Test device ID
ADMOB_TEST_DEVICE = "C4DD2E11E22D9AE28E0AFE23888A5F9D"

# AdMob banner ID
ADMOB_BANNER_ID = "ca-app-pub-1032896526805063/5239110122"