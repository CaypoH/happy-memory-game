HOW TO BUILD

# create a directory for keystore file
mkdir -p ~/playmarket_keys

# export env variables
export P4A_RELEASE_KEYSTORE=~/playmarket_keys/happy_memory_game.keystore
export P4A_RELEASE_KEYSTORE_PASSWD=<09password>
export P4A_RELEASE_KEYALIAS_PASSWD=<09password>
export P4A_RELEASE_KEYALIAS=happymemory

# build an APK
buildozer android release