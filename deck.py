from kivy.loader import Loader
from constants import *
from os import listdir
from os.path import isfile, join
import random

class Deck():
    def __init__(self):
        self.deck = [f for f in listdir(CARDS_IMAGES_FOLDER) if isfile(join(CARDS_IMAGES_FOLDER, f))]
        self.deck.remove(CARD_BACK)
        Loader.image(CARDS_IMAGES_FOLDER + CARD_BACK) # preloading card back image

    def get_card(self):
        return self.deck.pop(random.randrange(0, len(self.deck)))