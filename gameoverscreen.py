from functools import partial
from kivy.clock import Clock
from kivy.config import ConfigParser
from kivy.core.audio import SoundLoader
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from constants import *


class GameOverScreen(Screen):
    def __init__(self, **kwargs):
        super(GameOverScreen, self).__init__(**kwargs)
        self.conf = ConfigParser()

    def restart(self, *largs):
        self.startscreen.switch(self.startscreen.current_level())
        self.manager.current = 'game'

    def on_pre_enter(self, *args):
        self.conf.read(CONFIG_FILE)
        self.sound_on = self.conf.getint('gameconfig', 'sounds')
        # clearing all buttons like 'replay' or 'next level'. We will add a new ones again
        self.replaybutton.clear_widgets()
        self.new_achieve_grid.clear_widgets()
        self.achieve_sound = SoundLoader.load(MUSIC_FOLDER + 'new_achievement.ogg')

    def on_enter(self, *args):
        self.achieves = self.startscreen.get_achieve_list()
        # we are calling show_achieves method each second until it returns False
        Clock.schedule_interval(partial(self.show_achieves, self.achieves), 1)
        if self.width / float(self.height) == 0.625: # this is for mobiles with 16:10 screen ratio
            self.new_achieve_grid.col_default_width = self.width * .375

    def show_achieves(self, achieve, *largs):
        """
        if list of achieves is not empty we show one
        new one is going to be shown at next method call
        :param achieve: List of achievements 
        :param largs: 
        :return: True/False
        """

        if achieve:
            self.new_achieve_grid.add_widget(achieve.pop(0))
            if self.sound_on:
                self.achieve_sound.volume = .2
                self.achieve_sound.play()
            return True
        # if list is empty it means that we have shown all the achievements
        # so we return False which stops a schedule
        else:
            if self.startscreen.current_level() < 3:
                self.replaybutton.add_widget(Button(text='Play again',
                                                    font_size=self.height * .0227,
                                                    on_release=self.restart,
                                                    font_name=FONTS_FOLDER + MAIN_FONT,
                                                    border=[0,0,0,0],
                                                    background_normal=IMAGES_FOLDER + 'button.png',
                                                    background_down=IMAGES_FOLDER + 'button_pressed.png'))
                self.replaybutton.add_widget(Button(text='Next level',
                                                    font_size=self.height * .0227,
                                                    font_name=FONTS_FOLDER + MAIN_FONT,
                                                    border=[0, 0, 0, 0],
                                                    background_normal=IMAGES_FOLDER + 'button.png',
                                                    background_down=IMAGES_FOLDER + 'button_pressed.png',
                                                    on_release=partial(self.startscreen.switch,
                                                                       self.startscreen.current_level() + 1)))
            else:
                self.replaybutton.add_widget(Button(text='Play again',
                                                    font_size=self.height * .045,
                                                    on_release=self.restart,
                                                    font_name=FONTS_FOLDER + MAIN_FONT,
                                                    border=[0, 0, 0, 0],
                                                    background_normal=IMAGES_FOLDER + 'replay_button.png',
                                                    background_down=IMAGES_FOLDER + 'replay_button_pressed.png'))
            return False