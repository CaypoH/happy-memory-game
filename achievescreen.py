from kivy.config import ConfigParser
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from achieve_descr_modal import AchieveModal
from constants import *


class AchievementsScreen(Screen):
    def __init__(self, **kwargs):
        super(AchievementsScreen, self).__init__(**kwargs)
        self.config = ConfigParser()
        self.achieves = {}

    def on_pre_enter(self, *args):
        if self.width / float(self.height) == 0.625: # this is for mobiles with 16:10 screen ratio
            self.achievegrid.col_default_width = self.width * .25

    def open_achieve_screen(self):
        self.config.read(CONFIG_FILE)
        self.achieves = {'easy_5': self.config.getint('achievements', 'easy_5'),
                         'easy_4': self.config.getint('achievements', 'easy_4'),
                         'easy_3': self.config.getint('achievements', 'easy_3'),
                         'normal_10': self.config.getint('achievements', 'normal_10'),
                         'normal_9': self.config.getint('achievements', 'normal_9'),
                         'normal_8': self.config.getint('achievements', 'normal_8'),
                         'hard_16': self.config.getint('achievements', 'hard_16'),
                         'hard_15': self.config.getint('achievements', 'hard_15'),
                         'hard_14': self.config.getint('achievements', 'hard_14')}

        for key, value in sorted(self.achieves.iteritems()):
            # image name convention: %level%_%moves%_%is_achieved%
            # example: easy_3_0 (easy level, 3 moves, not achieved)
            self.achievegrid.add_widget(Button
                                        (id=key,
                                         background_down=IMAGES_FOLDER + key + '_' + str(value) + '.png',
                                         background_normal=IMAGES_FOLDER + key + '_' + str(value) + '.png',
                                         on_press=self.achieve_description,
                                         border=[0,0,0,0]
                                         )
                                        )

        self.manager.current = 'achieve'

    def achieve_description(self, instance):
        self.acheve_descr = AchieveModal(instance=instance.id)
        self.acheve_descr.open()