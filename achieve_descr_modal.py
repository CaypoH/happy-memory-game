from kivy.lang import Builder
from kivy.uix.modalview import ModalView

Builder.load_file('achieve_descr_modal.kv')

class AchieveModal(ModalView):
    def __init__(self, **kwargs):
        super(AchieveModal, self).__init__(**kwargs)
        self.achieve = kwargs['instance'].split('_')
        self.ids.achieve_text.text = '[color=000000]Open all cards at ' + \
                                     str(self.achieve[0]) + ' level in [color=df002f]' + str(self.achieve[1]) + \
                                     '[/color] [color=000000]moves[/color]'

    def close(self):
        self.dismiss()