from kivy.uix.modalview import ModalView


class SettingsPopup(ModalView):
    def close(self):
        self.dismiss()

    def save(self):
        self.music = self.ids.music_checkbox.state
        self.sound = self.ids.sounds_checkbox.state
        if self.music == 'down':
            self.startscreen.music_state(True)
        if self.music == 'normal':
            self.startscreen.music_state(False)
        if self.sound == 'down':
            self.startscreen.sound_state(True)
        if self.sound == 'normal':
            self.startscreen.sound_state(False)
        self.dismiss()