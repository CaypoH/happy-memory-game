import random
from kivy.lang import Builder
from kivy.config import ConfigParser
from kivy.core.audio import SoundLoader
from kivy.loader import Loader
from kivy.uix.image import Image
from kivy.uix.screenmanager import Screen
from constants import *
from deck import Deck
from cardbutton import CardButton
from kivmob import KivMob


Builder.load_file('settings_popup.kv')
Builder.load_file('quitpopup.kv')


class StartScreen(Screen):
    def __init__(self, **kwargs):
        super(StartScreen, self).__init__(**kwargs)
        self.ads = KivMob(ADMOB_APP_ID)
        self.ads.add_test_device(ADMOB_TEST_DEVICE)
        self.ads.new_banner({"unitID": ADMOB_BANNER_ID})
        self.ads.request_banner()
        self.conf = ConfigParser()
        self.conf.read(CONFIG_FILE)
        self.music_play = self.conf.getint('gameconfig', 'music')
        self.sound_on = self.conf.getint('gameconfig', 'sounds')
        self.game_state = 0
        self.first_card = self.second_card = None
        self.pairs = {1: 3, 2: 6, 3: 10} # level ratio to number of pairs of cards
        self.card_size = {1: [.416666,.234375], 2: [.277777, .15625], 3: [.2083333,.1171875]}
        self.achieves = []
        self.level_names = {1: 'easy', 2: 'normal', 3: 'hard'}
        self.music = SoundLoader.load(MUSIC_FOLDER + BACKGROUND_MUSIC)
        self.music.loop = True
        self.record = None
        if self.music_play:
            self.music.play()

    def clear_game_state(self):
        self.game_state = 0

    def ads_close_banner(self):
        self.ads.hide_banner()

    '''
    Settings related methods starts here
    '''
    def music_state(self, play):
        if play:
            self.music_play = True
            self.conf.set('gameconfig','music', 1)
            self.conf.write()
            self.music.play()
        else:
            self.music_play = False
            self.conf.set('gameconfig', 'music', 0)
            self.conf.write()
            self.music.stop()

    def sound_state(self, play):
        if play:
            self.sound_on = True
            self.conf.set('gameconfig','sounds', 1)
            self.conf.write()
        else:
            self.sound_on = False
            self.conf.set('gameconfig','sounds', 0)
            self.conf.write()

    def settings_popup(self):
        self.settings.ids.music_checkbox.state = 'down'
        self.settings.ids.sounds_checkbox.state = 'down'
        if not self.music_play:
            self.settings.ids.music_checkbox.state = 'normal'
        if not self.sound_on:
            self.settings.ids.sounds_checkbox.state = 'normal'
        self.settings.open()
    '''
    Settings related methods ends here
    '''

    def switch(self, level, *largs):
        self.deck = Deck()
        self.lst = []
        self.animals = {} # this list stores names of animals. We are going use it to load sounds
        self.achieves = []
        self.moves = 0
        self.level = level
        self.bestmoves = self.conf.getint('gameconfig', 'bestmoves_' + str(self.level))
        if self.width / float(self.height) == 0.625: # this is for mobiles with 16:10 screen ratio
            self.card_size = {1: [.375, .234375], 2: [.25, .15625], 3: [.1875, .1171875]}
        self.gamescreen.cards.clear_widgets()
        self.gamescreen.cards.cols = self.level + 1 # number of columns for our greedlayout
        self.gamescreen.ids.moves.text = 'Moves: [color=df002f]0[/color]'
        if self.bestmoves == 99:
            self.gamescreen.ids.best_moves.text = 'Best:'
        else:
            self.gamescreen.ids.best_moves.text = 'Best: [color=df002f]' + str(self.bestmoves) + '[/color]'
        # if game level is low we have less cards on the table so we can make them bigger
        self.gamescreen.cards.col_default_width = self.width * self.card_size[self.level][0]
        self.gamescreen.cards.row_default_height = self.height * self.card_size[self.level][1]

        for i in range(self.pairs[self.level]):
            self.card = self.deck.get_card()
            #print self.music_play
            if self.sound_on: # do not preload sound if it is turned off
                self.animals[self.card[:-4]] = SoundLoader.load(MUSIC_FOLDER + self.card[:-4] + '.ogg') # preloading sounds
            Loader.image(CARDS_IMAGES_FOLDER + self.card)  # preloading images of the cards we are going to use
            '''
            We append a card to a list two times.
            This is how we make sure that each card has a pair
            '''
            self.lst.append(CardButton(card_front=CARDS_IMAGES_FOLDER + self.card, card_back=CARDS_IMAGES_FOLDER + CARD_BACK))
            self.lst.append(CardButton(card_front=CARDS_IMAGES_FOLDER + self.card, card_back=CARDS_IMAGES_FOLDER + CARD_BACK))
        random.shuffle(self.lst) # mixing cards in a list so they are on random places
        if self.lst[0].card_value() == self.lst[1].card_value(): # if first two cards are still the same shuffle again
            random.shuffle(self.lst)

        for j in range(len(self.lst)):
            button = self.lst[j]
            button.bind(on_press=self.callback)
            self.gamescreen.cards.add_widget(button)

        self.manager.current = 'game'
        self.ads.show_banner()

    def gameover_popup(self):
        self.new_achieve_grid = self.gamescreen.gameoverscreen.ids.new_achieve_grid
        '''
        straight away method to track the achivements
        the logic is following:
        we collect all achievements to a list self.achieves
        wich can be obtained by calling get_achieve_list() method
        this method is going to be called from GameOverScreen instance later on
        so achieves are going to be showen one at a time
        '''
        if self.moves < self.bestmoves:
            self.conf.set('gameconfig','bestmoves_' + str(self.level), self.moves)
            self.gamescreen.ids.best_moves.text = 'Best: [color=df002f]' + str(self.moves) + '[/color]'
            self.achieves.append(Image(source=IMAGES_FOLDER + 'new_record.png'))


        # achievements for an easy level
        if self.level == 1 and self.moves <= 5:
            if self.conf.getint('achievements', 'easy_5') == 0:
                self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_5_1.png'))
            self.conf.set('achievements', 'easy_5', 1)
            if self.moves <= 4:
                if self.conf.getint('achievements', 'easy_4') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_4_1.png'))
                self.conf.set('achievements', 'easy_4', 1)
            if self.moves <= 3:
                if self.conf.getint('achievements', 'easy_3') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_3_1.png'))
                self.conf.set('achievements', 'easy_3', 1)

        # achievements for a normal level
        if self.level == 2 and self.moves <= 10:
            if self.conf.getint('achievements', 'normal_10') == 0:
                self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_10_1.png'))
            self.conf.set('achievements', 'normal_10', 1)
            if self.moves <= 9:
                if self.conf.getint('achievements', 'normal_9') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_9_1.png'))
                self.conf.set('achievements', 'normal_9', 1)
            if self.moves <= 8:
                if self.conf.getint('achievements', 'normal_8') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_8_1.png'))
                self.conf.set('achievements', 'normal_8', 1)

        # achievements for a hard level
        if self.level == 3 and self.moves <= 16:
            if self.conf.getint('achievements', 'hard_16') == 0:
                self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_16_1.png'))
            self.conf.set('achievements', 'hard_16', 1)
            if self.moves <= 15:
                if self.conf.getint('achievements', 'hard_15') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_15_1.png'))
                self.conf.set('achievements', 'hard_15', 1)
            if self.moves <= 14:
                if self.conf.getint('achievements', 'hard_14') == 0:
                    self.achieves.append(Image(source=IMAGES_FOLDER + self.level_names[self.level] + '_14_1.png'))
                self.conf.set('achievements', 'hard_14', 1)

        self.conf.write()
        self.ads.hide_banner()
        self.manager.current = 'gameover'

    def get_achieve_list(self):
        return self.achieves

    def current_level(self):
        return self.level

    def callback(self, instance):
        self.instance = instance
        self.moves_label = self.gamescreen.ids.moves
        self.exposed = 0
        if not self.instance.pressed:
            if self.game_state == 0:
                self.first_card = self.instance
                self.game_state = 1
            elif self.game_state == 1:
                self.second_card = self.instance
                self.game_state = 2
                self.moves += 1
                if self.first_card.card_value() == self.second_card.card_value():
                    if self.sound_on:
                        self.animals[self.first_card.card_value()[len(CARDS_IMAGES_FOLDER):-4]].volume = .2
                        self.animals[self.first_card.card_value()[len(CARDS_IMAGES_FOLDER):-4]].play()
                self.moves_label.text = 'Moves: [color=df002f]' + str(self.moves) + '[/color]'
            else:
                if not self.first_card.card_value() == self.second_card.card_value():
                    self.first_card.pressed = None
                    self.second_card.pressed = None
                    self.first_card.flip_back()
                    self.second_card.flip_back()
                self.first_card = instance
                self.game_state = 1
            for i in range(len(self.lst)):
                if self.lst[i].pressed:
                    self.exposed += 1
                    if len(self.lst) == (self.exposed + 1):
                        self.gameover_popup()