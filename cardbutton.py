from kivy.uix.button import Button

class CardButton(Button):
    def __init__(self, **kwargs):
        super(CardButton, self).__init__(**kwargs)
        self.card_front = kwargs['card_front']
        self.card_back = kwargs['card_back']
        self.background_down = self.card_front
        self.background_normal = self.card_back
        self.pressed = None
        self.opacity = 1
        self.border = [0, 0, 0, 0]

    def on_press(self):
        if not self.pressed:
            self.background_normal = self.background_down
            self.pressed = True

    def card_value(self):
        return self.card_front

    def flip_back(self):
        self.background_normal = self.card_back

    '''
    This method makes a card invisible
    Use case: if opened cards are paired they need to become transparent
    '''
    def make_invisible(self):
        self.opacity = 0